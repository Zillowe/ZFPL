<div align="center">
<h1>Zillowe Foundation Public License</h1>
Date: February 16, 2025 <br>
Version: 1.0.2
</div>


## Summary

- You can freely use, modify, and distribute the software for any purpose, including commercial use.

- You must include the copyright notice and permission notice in your copies.

- You can't use the copyright holder's name for promotion without permission.

- You must attribute the original software in derivative works (basically, give credit).

- If you distribute a modified version, you should try to make sure it's also distributed under an open-source license similar to this one. (This is the "contagious" clause).

- The software is provided without warranty, and the copyright holders are not liable for any damages.

- This license can be revised in the future, but you can choose which version to use.



## Badges

![ZFPL](https://codeberg.org/Zillowe/ZFPL/raw/branch/main/badges/1-0/dark.svg) | Dark

![ZFPL](https://codeberg.org/Zillowe/ZFPL/raw/branch/main/badges/1-0/light.svg) | Light


You can use them in repository to show that you're using this license, just add it to your files:


`README.md`

```markdown
<!-- dark -->

![ZFPL](https://codeberg.org/Zillowe/ZFPL/raw/branch/main/badges/1-0/dark.svg)

<!-- light -->

![ZFPL](https://codeberg.org/Zillowe/ZFPL/raw/branch/main/badges/1-0/light.svg)
```

`index.html`

```html
<!-- dark -->

<img
  alt="ZFPL"
  src="https://codeberg.org/Zillowe/ZFPL/raw/branch/main/badges/1-0/dark.svg"
/>

<!-- light -->

<img
  alt="ZFPL"
  src="https://codeberg.org/Zillowe/ZFPL/raw/branch/main/badges/1-0/light.svg"
/>
```