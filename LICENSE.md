# Zillowe Foundation Public License
Date: February 16, 2025 <br>
Version: 1.0.2

## 1. Definitions

1.1. "The Materials" – refers to the software and any associated documentation files. <br>
1.2. "Derivative Work" – refers to any modification, adaptation, or transformation of The Materials. <br>
1.3. "Licensee" – refers to any person who legally obtains a copy of The Materials. <br>
1.4. "Licensor" – refers to the copyright holder(s) of The Materials.

## 2. Grant of Rights

2.1. Permission is granted, free of charge, to any Licensee to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of The Materials. <br>
2.2. The Licensee may allow others to use The Materials under the same conditions as stated in this license.

## 3. Attribution Requirement

3.1. The Licensee must preserve all copyright notices and author attributions from the original version of The Materials in all copies and Derivative Works.

## 4. Distribution Conditions

4.1. If the Licensee distributes The Materials or any Derivative Work, they must make reasonable efforts to ensure it is distributed under a license that permits free use, modification, and distribution, such as this license.

## 5. Trademark and Branding Restrictions

5.1. The Licensee may not use the Licensor’s name, trademarks, or branding for promotional or marketing purposes without explicit written permission.

## 6. Warranty Disclaimer

6.1. THE MATERIALS ARE PROVIDED "AS IS", WITHOUT ANY WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NONINFRINGEMENT. <br>
6.2. UNDER NO CIRCUMSTANCES SHALL THE AUTHORS OR LICENSORS BE LIABLE FOR ANY CLAIMS, DAMAGES, OR OTHER LIABILITIES ARISING FROM THE USE OR INABILITY TO USE THE MATERIALS.

## 7. License Revisions

7.1. This license may be revised by the Licensor. <br>
7.2. Licensor may choose to continue using this version or adopt a newer revision when available. <br>
7.3. Future revisions will be published at [https://codeberg.org/Zillowe/ZFPL].